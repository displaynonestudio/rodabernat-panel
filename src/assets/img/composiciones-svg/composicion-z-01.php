<style>

/***************************************************
 * Generated by SVG Artista on 7/24/2020, 12:40:29 PM
 * MIT license (https://opensource.org/licenses/MIT)
 * W. https://svgartista.net
 **************************************************/

@-webkit-keyframes animate-svg-stroke-1 {
  0% {
    stroke-dashoffset: 871.06982421875px;
    stroke-dasharray: 871.06982421875px;
  }

  100% {
    stroke-dashoffset: 0;
  }
}

@keyframes animate-svg-stroke-1 {
  0% {
    stroke-dashoffset: 871.06982421875px;
    stroke-dasharray: 871.06982421875px;
  }

  100% {
    stroke-dashoffset: 0;
  }
}

.stockholm-z-01-1 {
  -webkit-animation: animate-svg-stroke-1 3s cubic-bezier(0.47, 0, 0.745, 0.715) 0s both;
          animation: animate-svg-stroke-1 3s cubic-bezier(0.47, 0, 0.745, 0.715) 0s both;
}

@-webkit-keyframes animate-svg-stroke-2 {
  0% {
    stroke-dashoffset: 15.425999641418457px;
    stroke-dasharray: 15.425999641418457px;
  }

  100% {
    stroke-dashoffset: 0;
  }
}

@keyframes animate-svg-stroke-2 {
  0% {
    stroke-dashoffset: 15.425999641418457px;
    stroke-dasharray: 15.425999641418457px;
  }

  100% {
    stroke-dashoffset: 0;
  }
}

.stockholm-z-01-2 {
  -webkit-animation: animate-svg-stroke-2 3s cubic-bezier(0.47, 0, 0.745, 0.715) 0.12s both;
          animation: animate-svg-stroke-2 3s cubic-bezier(0.47, 0, 0.745, 0.715) 0.12s both;
}

@-webkit-keyframes animate-svg-stroke-3 {
  0% {
    stroke-dashoffset: 9.34499979019165px;
    stroke-dasharray: 9.34499979019165px;
  }

  100% {
    stroke-dashoffset: 0;
  }
}

@keyframes animate-svg-stroke-3 {
  0% {
    stroke-dashoffset: 9.34499979019165px;
    stroke-dasharray: 9.34499979019165px;
  }

  100% {
    stroke-dashoffset: 0;
  }
}

.stockholm-z-01-3 {
  -webkit-animation: animate-svg-stroke-3 3s cubic-bezier(0.47, 0, 0.745, 0.715) 0.24s both;
          animation: animate-svg-stroke-3 3s cubic-bezier(0.47, 0, 0.745, 0.715) 0.24s both;
}

@-webkit-keyframes animate-svg-stroke-4 {
  0% {
    stroke-dashoffset: 23.844999313354492px;
    stroke-dasharray: 23.844999313354492px;
  }

  100% {
    stroke-dashoffset: 0;
  }
}

@keyframes animate-svg-stroke-4 {
  0% {
    stroke-dashoffset: 23.844999313354492px;
    stroke-dasharray: 23.844999313354492px;
  }

  100% {
    stroke-dashoffset: 0;
  }
}

.stockholm-z-01-4 {
  -webkit-animation: animate-svg-stroke-4 3s cubic-bezier(0.47, 0, 0.745, 0.715) 0.36s both;
          animation: animate-svg-stroke-4 3s cubic-bezier(0.47, 0, 0.745, 0.715) 0.36s both;
}

</style>

<!--?xml version="1.0" encoding="UTF-8" standalone="no"?-->
<svg class="checkbox ml-4 pr-4 zoc-01" height="80" viewBox="0 0 108 56" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" xmlns:serif="http://www.serif.com/" style="fill-rule:evenodd;clip-rule:evenodd;stroke-linecap:round;stroke-linejoin:round;">
    <g transform="matrix(1,0,0,1,85.495,55.678)">
        <path d="M0,-55.553L13.704,-49.78L13.485,-22.54L-71.681,-8.665L-85.37,-14.421L-85.136,-41.669L0,-55.553ZM13.673,-49.758L13.122,-49.689L13.094,-44.708C13.094,-44.708 13.056,-44.014 12.47,-43.967C11.886,-43.923 -70.28,-30.473 -70.28,-30.473C-70.28,-30.473 -70.89,-30.333 -70.886,-30.976C-70.882,-31.617 -70.811,-35.968 -70.811,-35.968L-71.428,-35.848M-84.245,-41.667L-70.82,-35.994M-85.044,-41.628L-71.428,-35.848L-71.681,-8.665M12.329,-43.909L12.329,-22.367M-0.587,-55.429L13.122,-49.689M-15.118,-39.541L-15.118,-17.857M-42.73,-34.999L-42.73,-13.359L-68.847,-9.087L-71.349,-8.677L-68.925,-7.721L-68.894,-7.707L-57.603,-9.5M-68.894,-7.707L-68.894,-9.087M-68.417,-7.739L-68.417,-5.348L-68.99,-5.241L-70.189,-5.74L-70.189,-8.107M-61.75,-10.179L-60.535,-9.663L-57.381,-10.209C-57.381,-10.209 -56.422,-10.338 -56.359,-9.141M-57.453,-10.963C-57.453,-10.963 -55.902,-10.883 -55.898,-9.408C-55.894,-8.318 -55.977,-0.107 -55.977,-0.107L-56.513,0L-57.708,-0.517L-57.629,-9.408C-57.629,-9.408 -57.542,-10.034 -57.752,-10.132M-55.911,-9.811L-14.406,-16.539L-3.57,-18.299M-14.401,-16.444L-14.401,-14.145L-14.954,-14.038L-16.158,-14.539L-16.158,-16.156M-1.663,-18.644L8.198,-20.24L8.198,-21.656M-2.897,-19.867C-3.637,-19.253 -3.55,-18.136 -3.55,-18.136L-3.683,-9.337L-2.464,-8.794L-1.965,-8.916C-1.965,-8.916 -1.834,-17.353 -1.841,-18.136C-1.85,-18.918 -0.967,-19.365 -0.967,-19.365L2.31,-19.894L2.304,-20.499L2.014,-20.632M-70.28,-30.473L-70.28,-8.991M-74.201,-37.532L-0.356,-49.56C-0.356,-49.56 0.091,-49.594 0.101,-50.022C0.108,-50.273 0.136,-55.06 0.136,-55.06L-83.485,-41.439M-0.415,-49.512L12.335,-43.985" style="fill: none; fill-rule: nonzero; stroke: black; stroke-width: 0.25px;" class="stockholm-animate"></path>
    </g>
    <g transform="matrix(0,1,1,0,112.013,40.0904)">
        <path d="M-6.713,-6.713L6.713,-6.713" style="fill: none; fill-rule: nonzero; stroke: rgb(102, 102, 102); stroke-width: 0.25px; stroke-linecap: butt; stroke-linejoin: miter;" class="stockholm-animate"></path>
    </g>
    <g transform="matrix(1,0,0,1,99.8554,33.3867)">
        <path d="M0,0L7.345,0" style="fill: none; fill-rule: nonzero; stroke: rgb(102, 102, 102); stroke-width: 0.25px; stroke-linecap: butt; stroke-linejoin: miter;" class="stockholm-animate"></path>
    </g>
    <g transform="matrix(1,0,0,1,85.3554,46.8027)">
        <path d="M0,0L21.845,0" style="fill: none; fill-rule: nonzero; stroke: rgb(102, 102, 102); stroke-width: 0.25px; stroke-linecap: butt; stroke-linejoin: miter;" class="stockholm-animate"></path>
    </g>
</svg>

<style>




</style>







<!--?xml version="1.0" encoding="UTF-8" standalone="no"?-->

<svg class="checkbox ml-4 comp-04" viewBox="0 0 306 66" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" xmlns:serif="http://www.serif.com/" style="fill-rule:evenodd;clip-rule:evenodd;stroke-linecap:round;stroke-linejoin:round;">
    <g id="Mesa-de-trabajo1" serif:id="Mesa de trabajo1" transform="matrix(1,0,0,1,1.52482,2.85491)">
        <rect x="-1.525" y="-2.855" width="305.018" height="65.316" style="fill: none;" class="stockholm-animate"></rect>
        <g transform="matrix(1,0,0,1,0.475176,0.145092)">
            <path d="M49.405,37.54L111.643,27.511M127.607,7.346L142.56,13.82M13.369,25.839L127.677,7.289C127.677,7.289 128.2,7.249 128.213,6.748C128.221,6.453 128.251,0.847 128.251,0.847L2.479,21.262M17.967,34.104L17.967,59.275M49.3,28.992L49.3,54.282M80.288,23.962L80.288,49.208M111.67,18.913L111.67,44.136M127.404,0.413L143.482,7.138M142.556,13.907L142.556,39.14M0.648,21.039L16.62,27.81L16.323,59.654M1.588,20.993L17.335,27.639M144.131,7.057L143.482,7.138L143.451,12.971C143.451,12.971 143.403,13.786 142.72,13.838C142.035,13.893 17.968,34.105 17.968,34.105C17.968,34.105 17.254,34.268 17.258,33.518C17.262,32.768 17.346,27.67 17.346,27.67L16.622,27.811M128.093,0.266L144.167,7.03L143.907,38.943L16.323,59.654L0.269,52.91L0.541,20.991L128.093,0.266Z" style="fill: none; fill-rule: nonzero; stroke: black; stroke-width: 0.25px;" class="stockholm-animate"></path>
        </g>
        <g transform="matrix(1,0,0,1,0.475176,0.145092)">
            <path d="M175.438,58.486L173.855,58.743L158.66,52.378L158.921,22.23L279.643,2.66L294.853,9.044L294.608,39.184L293.331,39.369M274.774,42.396L272.625,42.747M210.053,52.882L204.253,53.82M294.818,9.071L294.209,9.146L294.177,14.654C294.177,14.654 294.133,15.429 293.478,15.474C292.833,15.527 175.409,34.614 175.409,34.614C175.409,34.614 174.736,34.769 174.74,34.058C174.742,33.35 174.821,28.538 174.821,28.538L174.136,28.672M159.911,22.231L174.809,28.508M159.019,22.278L174.136,28.673L173.856,58.743M293.33,15.542L293.33,39.368M278.988,2.799L294.209,9.147M175.408,34.614L175.408,58.383M171.061,26.806L279.242,9.29C279.242,9.29 279.743,9.252 279.753,8.78C279.762,8.499 279.792,3.205 279.792,3.205L160.752,22.487M279.178,9.341L293.335,15.46" style="fill: none; fill-rule: nonzero; stroke: black; stroke-width: 0.35px;" class="stockholm-animate"></path>
        </g>
        <g transform="matrix(1,0,0,1,0.475176,0.145092)">
            <path d="M175.408,34.681L203.776,33.75L203.882,57.177M203.883,57.177L175.575,58.473" style="fill: none; fill-rule: nonzero; stroke: black; stroke-width: 0.25px;" class="stockholm-animate"></path>
        </g>
        <g transform="matrix(1,0,0,1,0.475176,0.145092)">
            <path d="M279.101,17.808L279.101,22.084M210.099,52.983L210.078,29.377" style="fill: none; fill-rule: nonzero; stroke: black; stroke-width: 0.25px;" class="stockholm-animate"></path>
        </g>
        <g transform="matrix(1,0,0,1,0.475176,0.145092)">
            <path d="M240.82,27.029L240.82,24.059M210.077,28.973L210.077,30.608" style="fill: none; fill-rule: nonzero; stroke: black; stroke-width: 0.25px;" class="stockholm-animate"></path>
        </g>
        <g transform="matrix(1,0,0,1,0.475176,0.145092)">
            <path d="M204.02,50.434L209.58,52.75" style="fill: none; fill-rule: nonzero; stroke: black; stroke-width: 0.35px;" class="stockholm-animate"></path>
        </g>
        <g transform="matrix(1,0,0,1,0.475176,0.145092)">
            <path d="M195.515,31.406L195.515,33.998" style="fill: none; fill-rule: nonzero; stroke: black; stroke-width: 0.25px;" class="stockholm-animate"></path>
        </g>
        <g transform="matrix(1,0,0,1,0.475176,0.145092)">
            <path d="M213.52,40.453L242.064,35.841L242.064,49.939L213.509,54.56L213.52,40.453ZM241.682,34.791L213.127,39.414L213.139,31.523M210.074,38.263L213.123,39.456M210.245,34.107L213.123,35.25M209.958,53.037L213.502,54.469M210.162,46.762L213.509,48.111" style="fill: none; fill-rule: nonzero; stroke: black; stroke-width: 0.25px;" class="stockholm-animate"></path>
        </g>
        <g transform="matrix(1,0,0,1,0.475176,0.145092)">
            <path d="M269.145,19.444L269.145,22.103" style="fill: none; fill-rule: nonzero; stroke: black; stroke-width: 0.35px;" class="stockholm-animate"></path>
        </g>
        <g transform="matrix(1,0,0,1,0.475176,0.145092)">
            <path d="M241.747,34.798L241.756,26.91M241.756,26.908L213.139,31.525M274.796,47.611L293.349,39.382" style="fill: none; fill-rule: nonzero; stroke: black; stroke-width: 0.25px;" class="stockholm-animate"></path>
        </g>
        <g transform="matrix(1,0,0,1,0.475176,0.145092)">
            <path d="M274.812,47.622L274.816,23.998M274.812,23.998L293.349,15.75M272.279,29.866L272.292,21.962M272.289,21.96L243.693,26.586M244.076,49.631L244.089,35.503L272.67,30.885L272.67,45.005L244.076,49.631ZM272.289,29.832L243.693,34.46L243.708,26.558M241.801,33.907L243.69,34.502M241.785,29.717L243.689,30.291M242.178,48.695L244.07,49.54M242.178,42.41L244.076,43.175" style="fill: none; fill-rule: nonzero; stroke: black; stroke-width: 0.25px;" class="stockholm-animate"></path>
        </g>
    </g>
</svg>



<style>

/***************************************************
 * Generated by SVG Artista on 7/24/2020, 12:53:21 PM
 * MIT license (https://opensource.org/licenses/MIT)
 * W. https://svgartista.net
 **************************************************/

@-webkit-keyframes animate-svg-stroke-1 {
  0% {
    stroke-dashoffset: 630.1127319335938px;
    stroke-dasharray: 630.1127319335938px;
  }

  100% {
    stroke-dashoffset: 0;
  }
}

@keyframes animate-svg-stroke-1 {
  0% {
    stroke-dashoffset: 630.1127319335938px;
    stroke-dasharray: 630.1127319335938px;
  }

  100% {
    stroke-dashoffset: 0;
  }
}

.stockholm-m-00-1 {
  -webkit-animation: animate-svg-stroke-1 3.5s cubic-bezier(0.47, 0, 0.745, 0.715) 0s both;
          animation: animate-svg-stroke-1 3.5s cubic-bezier(0.47, 0, 0.745, 0.715) 0s both;
}

@-webkit-keyframes animate-svg-stroke-2 {
  0% {
    stroke-dashoffset: 736.4069213867188px;
    stroke-dasharray: 736.4069213867188px;
  }

  100% {
    stroke-dashoffset: 0;
  }
}

@keyframes animate-svg-stroke-2 {
  0% {
    stroke-dashoffset: 736.4069213867188px;
    stroke-dasharray: 736.4069213867188px;
  }

  100% {
    stroke-dashoffset: 0;
  }
}

.stockholm-animate {
  -webkit-animation: animate-svg-stroke-1 3.5s cubic-bezier(0.47, 0, 0.745, 0.715) 0s both;
          animation: animate-svg-stroke-1 3.5s cubic-bezier(0.47, 0, 0.745, 0.715) 0s both;
}

</style>



<!--?xml version="1.0" encoding="UTF-8" standalone="no"?-->

<svg class="checkbox ml-4 comp-01" viewBox="0 0 301 60" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" xmlns:serif="http://www.serif.com/" style="fill-rule:evenodd;clip-rule:evenodd;stroke-linecap:round;stroke-linejoin:round;">
    <g id="Mesa-de-trabajo1" serif:id="Mesa de trabajo1" transform="matrix(1,0,0,1,1.35635,-0.32885)">
        <rect x="-1.356" y="0.329" width="300.428" height="59.515" style="fill: none;" class="stockholm-animate"></rect>
        <clipPath id="_clip1">
            <rect x="-1.356" y="0.329" width="300.428" height="59.515" class="stockholm-animate"></rect>
        </clipPath>
        <g clip-path="url(#_clip1)">
            <g transform="matrix(1,0,0,1,0.643653,0.32885)">
                <path d="M80.474,-0.162L99.474,7.834L99.167,45.57L19.314,58.838L0.337,50.865L0.658,13.116L80.474,-0.162ZM99.432,7.865L98.668,7.963L98.627,14.859C98.627,14.859 98.573,15.827 97.764,15.881C96.955,15.947 21.262,28.625 21.262,28.625C21.262,28.625 20.416,28.817 20.423,27.932C20.428,27.043 20.524,21.016 20.524,21.016L19.669,21.179M1.568,12.977L20.512,20.977M0.788,13.174L19.668,21.179L19.314,58.838M97.572,15.969L97.572,45.808M79.661,0.009L98.667,7.963M59.465,22.357L59.465,52.132M21.261,28.625L21.261,58.522M15.825,18.846L79.98,8.137C79.98,8.137 80.598,8.09 80.614,7.5C80.622,7.148 80.66,0.522 80.66,0.522L2.758,13.48M79.896,8.203L97.578,15.863" style="fill: none; fill-rule: nonzero; stroke: black; stroke-width: 0.25px;" class="stockholm-animate"></path>
            </g>
            <g transform="matrix(1,0,0,1,-15.3563,0.32885)">
                <path d="M215.222,54.026L215.347,29.723L184.328,28.242L184.149,56.588L215.199,58.333L215.222,54.026Z" style="fill: none; fill-rule: nonzero; stroke: black; stroke-width: 0.25px; stroke-miterlimit: 10;" class="stockholm-animate"></path>
            </g>
            <g transform="matrix(1,0,0,1,-15.3563,0.32885)">
                <path d="M224.13,38.886L215.393,40.427M184.126,56.595L182.289,56.915L165.174,49.291L165.465,13.189L237.445,0.489L254.58,8.136L254.306,44.23L252.804,44.491M224.041,49.562L215.393,51.084M254.541,8.166L253.853,8.258L253.814,14.855C253.814,14.855 253.769,15.778 253.037,15.834C252.309,15.894 184.043,28.021 184.043,28.021C184.043,28.021 183.28,28.208 183.289,27.359C183.294,26.51 183.381,20.745 183.381,20.745L182.606,20.903M166.286,13.056L183.368,20.705M165.582,13.242L182.604,20.902L182.289,56.915M252.861,15.917L252.861,44.456M236.711,0.652L253.853,8.257M236.919,18.651L236.927,22.49M184.042,28.021L184.042,56.616M179.143,18.669L236.997,8.428C236.997,8.428 237.556,8.383 237.57,7.817C237.575,7.484 237.612,1.143 237.612,1.143L167.358,13.538M236.924,8.49L252.868,15.814" style="fill: none; fill-rule: nonzero; stroke: black; stroke-width: 0.25px;" class="stockholm-animate"></path>
            </g>
            <g transform="matrix(1,0,0,1,-15.3563,0.32885)">
                <path d="M224.08,52.563L224.041,56.892L252.861,44.452L252.84,16.055L224.296,27.703L224.08,52.563Z" style="fill: none; fill-rule: nonzero; stroke: black; stroke-width: 0.25px; stroke-linecap: butt; stroke-linejoin: miter; stroke-miterlimit: 10;" class="stockholm-animate"></path>
            </g>
        </g>
    </g>
</svg>


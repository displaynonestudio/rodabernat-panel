<style>




</style>




<!--?xml version="1.0" encoding="UTF-8" standalone="no"?-->

<svg class="checkbox ml-4 comp-04" viewBox="0 0 330 60" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" xmlns:serif="http://www.serif.com/" style="fill-rule:evenodd;clip-rule:evenodd;stroke-linecap:round;stroke-linejoin:round;">
    <g id="Capa-1" serif:id="Capa 1" transform="matrix(1,0,0,1,-0.122,-0.1209)">
        <g transform="matrix(1,0,0,1,146.043,59.5699)">
            <path d="M0,-59.189L14.859,-52.978M0.184,-52.784L14.004,-46.797M0.78,-58.793L-144.006,-36.193M-133.943,-31.96L0.246,-52.835C0.246,-52.835 0.729,-52.869 0.745,-53.334C0.75,-53.61 0.78,-58.793 0.78,-58.793M-129.693,-24.323L-130.119,-0.094M-100.733,-29.049L-100.716,-4.866M-72.093,-33.333L-72.073,-9.528M-43.087,-37.866L-43.069,-13.697M14.004,-46.797L14.033,-22.687M-145.7,-36.396L-130.937,-30.138L-131.422,0.135M-144.831,-36.44L-130.277,-30.295M15.458,-53.047L14.859,-52.978L14.829,-47.582C14.829,-47.582 14.786,-46.83 14.152,-46.782C13.519,-46.73 -129.693,-24.323 -129.693,-24.323C-129.693,-24.323 -130.352,-24.169 -130.348,-24.865C-130.346,-25.556 -130.267,-30.271 -130.267,-30.271L-130.937,-30.138M0.634,-59.324L15.488,-53.074L15.264,-22.826L-131.422,0.135L-145.767,-6.722L-145.796,-36.444L0.634,-59.324ZM-14.543,-42.09L-14.526,-18.277" style="fill: none; fill-rule: nonzero; stroke: black; stroke-width: 0.25px;" class="stockholm-animate"></path>
        </g>
        <g transform="matrix(1,0,0,1,315.627,56.1775)">
            <path d="M0,-53.74L13.514,-48.097M0.169,-47.923L12.736,-42.491M0.713,-53.378L-130.942,-32.86M-121.789,-29.018L0.227,-47.972C0.227,-47.972 0.666,-48.004 0.679,-48.424C0.686,-48.674 0.713,-53.378 0.713,-53.378M-117.925,-22.082L-118.315,-0.084M-91.594,-26.195L-91.578,-4.419M-65.55,-30.261L-65.535,-8.651M-39.176,-34.378L-39.162,-12.435M12.736,-42.491L12.765,-20.6M-132.48,-33.047L-119.057,-27.364L-119.5,0.122M-131.691,-33.086L-118.459,-27.507M14.058,-48.167L13.514,-48.097L13.486,-43.202C13.486,-43.202 13.446,-42.516 12.871,-42.472C12.297,-42.427 -117.925,-22.082 -117.925,-22.082C-117.925,-22.082 -118.525,-21.944 -118.521,-22.574C-118.521,-23.205 -118.449,-27.483 -118.449,-27.483L-119.057,-27.364M-118.315,-0.059L-119.5,0.122L-132.54,-6.103L-132.569,-33.088L0.579,-53.862M-89.394,-4.567L-93.358,-3.96M-11.051,-16.827L-14.203,-16.333M0.579,-53.862L14.086,-48.187L13.879,-20.723L12.399,-20.492M-13.224,-38.216L-13.206,-16.593" style="fill: none; fill-rule: nonzero; stroke: black; stroke-width: 0.25px;" class="stockholm-animate"></path>
        </g>
        <g transform="matrix(1,0,0,1,304.641,17.1822)">
            <path d="M0,22.141L-0.031,25.416L23.751,18.378L23.751,-3.275L0.177,3.333L0,22.141Z" style="fill: none; fill-rule: nonzero; stroke: black; stroke-width: 0.25px; stroke-linecap: butt; stroke-linejoin: miter; stroke-miterlimit: 10;" class="stockholm-animate"></path>
        </g>
        <g transform="matrix(1,0,0,1,301.436,19.6091)">
            <path d="M0,20.222L0.099,1.463L-24.777,2.238L-24.922,24.118L-0.019,23.547L0,20.222ZM14.184,-3.896L14.184,-2.14M3.231,10.786L0.996,11.246M-11.568,0.118L-11.568,1.826M0.099,19.448L0.996,19.962" style="fill: none; fill-rule: nonzero; stroke: black; stroke-width: 0.25px; stroke-miterlimit: 10;" class="stockholm-animate"></path>
        </g>
        <g transform="matrix(1,0,0,1,275.007,27.6472)">
            <path d="M0,16.302L0.103,-2.449L-24.748,-1.674L-24.895,20.193L-0.019,19.628L0,16.302ZM-11.121,-3.891L-11.121,-2.182M0.103,15.054L1.43,15.944" style="fill: none; fill-rule: nonzero; stroke: black; stroke-width: 0.25px; stroke-miterlimit: 10;" class="stockholm-animate"></path>
        </g>
        <g transform="matrix(0,-1,-1,0,260.945,36.9972)">
            <path d="M-10.868,10.868L10.868,10.868" style="fill: none; fill-rule: nonzero; stroke: black; stroke-width: 0.25px; stroke-linecap: butt; stroke-linejoin: miter; stroke-miterlimit: 10;" class="stockholm-animate"></path>
        </g>
        <g transform="matrix(1,0,0,1,222.24,32.2252)">
            <path d="M0,19.975L0.101,1.216L-24.775,1.991L-24.919,23.869L-0.021,23.301L0,19.975ZM14.378,-3.894L14.378,-2.477M3.954,10.893L1.776,11.249M-11.372,0.058L-11.372,1.488M0.101,18.713L1.474,19.505" style="fill: none; fill-rule: nonzero; stroke: black; stroke-width: 0.25px; stroke-miterlimit: 10;" class="stockholm-animate"></path>
        </g>
        <g transform="matrix(-0.99521,0.097757,0.097757,0.99521,275.092,44.0016)">
            <path d="M-1.357,-0.067L0.003,-0.067" style="fill: none; fill-rule: nonzero; stroke: black; stroke-width: 0.25px;" class="stockholm-animate"></path>
        </g>
        <g transform="matrix(-0.999606,0.0280808,0.0280808,0.999606,250.271,26.2518)">
            <path d="M-24.852,-0.349L0.005,-0.349" style="fill: none; fill-rule: nonzero; stroke: black; stroke-width: 0.25px; stroke-miterlimit: 10;" class="stockholm-animate"></path>
        </g>
        <g transform="matrix(1,0,0,1,226.287,29.3022)">
            <path d="M0,22.146L-0.032,25.418L23.745,18.551L23.745,-3.272L0.175,3.336L0,22.146Z" style="fill: none; fill-rule: nonzero; stroke: black; stroke-width: 0.25px; stroke-linecap: butt; stroke-linejoin: miter; stroke-miterlimit: 10;" class="stockholm-animate"></path>
        </g>
    </g>
</svg>

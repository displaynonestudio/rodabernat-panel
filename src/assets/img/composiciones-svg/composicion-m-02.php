<style>
  
/***************************************************
 * Generated by SVG Artista on 7/24/2020, 10:49:00 AM
 * MIT license (https://opensource.org/licenses/MIT)
 * W. https://svgartista.net
 **************************************************/

@-webkit-keyframes animate-svg-stroke-1 {
  0% {
    stroke-dashoffset: 816.9051513671875px;
    stroke-dasharray: 816.9051513671875px;
  }

  100% {
    stroke-dashoffset: 0;
  }
}

@keyframes animate-svg-stroke-1 {
  0% {
    stroke-dashoffset: 816.9051513671875px;
    stroke-dasharray: 816.9051513671875px;
  }

  100% {
    stroke-dashoffset: 0;
  }
}

.stockholm-m-02-1 {
  -webkit-animation: animate-svg-stroke-1 3.5s cubic-bezier(0.47, 0, 0.745, 0.715) 0s both;
          animation: animate-svg-stroke-1 3.5s cubic-bezier(0.47, 0, 0.745, 0.715) 0s both;
}

</style>



<!--?xml version="1.0" encoding="UTF-8" standalone="no"?-->

<svg class="checkbox ml-4 comp-02" viewBox="0 0 300 60" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" xmlns:serif="http://www.serif.com/" style="fill-rule:evenodd;clip-rule:evenodd;stroke-linecap:round;stroke-linejoin:round;">
    <g transform="matrix(1,0,0,1,101.046,59.3548)">
        <path d="M0,-55.263L16.176,-48.458L15.915,-16.352L-84.61,0L-100.765,-6.785L-100.492,-38.898L0,-55.263ZM16.14,-48.434L15.487,-48.351L15.454,-42.483C15.454,-42.483 15.409,-41.664 14.717,-41.61C14.029,-41.554 -82.955,-25.706 -82.955,-25.706C-82.955,-25.706 -83.673,-25.542 -83.669,-26.297C-83.667,-27.052 -83.582,-32.181 -83.582,-32.181L-84.31,-32.038M-99.437,-38.896L-83.59,-32.211M-100.382,-38.85L-84.31,-32.038L-84.61,0M14.553,-41.539L14.553,-16.151M-0.692,-55.118L15.487,-48.351M-17.844,-36.391L-17.844,-10.833M-50.436,-31.038L-50.436,-5.534M-82.955,-25.706L-82.955,-0.383M-87.582,-34.023L-0.421,-48.2C-0.421,-48.2 0.106,-48.24 0.119,-48.742C0.128,-49.042 0.16,-54.681 0.16,-54.681L-98.541,-38.627M-0.49,-48.143L14.56,-41.626" style="fill: none; fill-rule: nonzero; stroke: black; stroke-width: 0.25px;" class="stockholm-animate"></path>
    </g>
    <g transform="matrix(1,0,0,1,253.506,49.9896)">
        <path d="M0,-40.723L14.937,-34.252M-86.452,-26.703L0.065,-40.776C0.065,-40.776 0.589,-40.82 0.606,-41.321C0.61,-41.612 0.639,-47.213 0.639,-47.213L-97.329,-31.275M-81.861,-18.451L-81.861,6.684M-0.206,-47.644L15.855,-40.926M14.932,-34.165L14.932,-8.964M-99.161,-31.497L-83.205,-24.735L-83.503,7.065M-98.225,-31.541L-82.492,-24.908M16.503,-41.009L15.855,-40.926L15.827,-35.102C15.827,-35.102 15.78,-34.29 15.094,-34.238C14.411,-34.183 -81.861,-18.451 -81.861,-18.451C-81.861,-18.451 -82.572,-18.286 -82.57,-19.035C-82.57,-19.785 -82.48,-24.876 -82.48,-24.876L-83.205,-24.735M-16.006,-3.912L-19.66,-3.32M-81.861,6.684L-83.503,7.065L-99.542,0.333L-99.271,-31.544L0.486,-47.788L16.542,-41.038L16.284,-9.166L14.937,-8.945" style="fill: none; fill-rule: nonzero; stroke: black; stroke-width: 0.25px;" class="stockholm-animate"></path>
    </g>
    <g transform="matrix(1,0,0,1,203.398,17.4153)">
        <path d="M0,38.112L-31.754,39.261M-31.754,14.124L-0.118,13L0,38.112M34.104,32.133L65.122,23.494M65.223,-1.149L34.104,7.282L34.104,32.133M30.541,33.029L0,34.209M30.354,8.155L30.541,33.029" style="fill: none; fill-rule: nonzero; stroke: black; stroke-width: 0.25px;" class="stockholm-animate"></path>
    </g>
    <g transform="matrix(1,0,0,1,233.41,30.3824)">
        <path d="M0,-4.814L-31.491,-3.779M-31.491,-3.779L-31.491,0.081M19.651,-11.893L19.651,-9.902M4.092,6.47C3.407,6.527 0.342,7.079 0.342,7.079M-45.889,-1.436L-45.889,0.592" style="fill: none; fill-rule: nonzero; stroke: black; stroke-width: 0.25px;" class="stockholm-animate"></path>
    </g>
    <rect id="Página-2" serif:id="Página 2" x="0" y="0" width="300" height="60" style="fill: none;" class="stockholm-animate"></rect>
</svg>


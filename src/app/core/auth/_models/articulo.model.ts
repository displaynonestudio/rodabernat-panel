import { BaseModel } from '../../_base/crud';

export class Articulo extends BaseModel {
    id: number;
    descripcion: string;
    venta: number;
    unidad: number;
    sd: number;
    sr: number;
    tipo: number;
    stock: number;
    iva: number;
    coste: number;
    produccion: number;
    
    clear(): void {
        this.id = undefined;
        this.descripcion = '';
        this.venta = 0;
        this.unidad = 0;
        this.sd = 0;
        this.sr = 0;
        this.tipo = 0;
        this.stock = 0;
        this.iva = 0;
        this.coste = 0;
        this.produccion = 0;
    }
}

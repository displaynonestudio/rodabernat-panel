import { BaseModel } from '../../_base/crud';
export class Presupuesto extends BaseModel {
    id: number;
    fecha: string;
    validez: string;
    cliente: string;
    descripcion: string;
    direccionTrabajo: string;
    datos: string;

    clear(): void {
        this.id = undefined;
        this.fecha = '';
        this.validez = '';
        this.cliente = '';
        this.descripcion = '';
        this.direccionTrabajo = '';
        this.datos = '';

    }
}

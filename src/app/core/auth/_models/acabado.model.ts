import { BaseModel } from '../../_base/crud';

export class Acabado extends BaseModel {
    id: number;
    grupo: string;
    codigoColor: string;
    descripcionEspanol: string;
    descripcionIngles: string;
    imagenes: string;

    clear(): void {
        this.id = undefined;
        this.grupo = '';
        this.codigoColor = '';
        this.descripcionEspanol = '';
        this.descripcionIngles = '';
        this.imagenes = '';
    }
}

import { BaseModel } from '../../_base/crud';
import { Address } from './address.model';
import { SocialNetworks } from './social-networks.model';

export class Rol extends BaseModel {
    id: number;
    rol: string;

    clear(): void {
        this.id = undefined;
        this.rol = '';

    }
}

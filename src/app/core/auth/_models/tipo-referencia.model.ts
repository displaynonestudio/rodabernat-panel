import { BaseModel } from '../../_base/crud';

export class TipoReferencia extends BaseModel {
    id: number;
    nombre: string;
	
    clear(): void {
        this.id = undefined;
        this.nombre = '';

    }
}

import { BaseModel } from '../../_base/crud';

export class Pedido extends BaseModel {
    id: number;
    fecha: Date;
    idUsuario: string;
    seleccion: string;
    importeTotal: number;

    clear(): void {
        this.id = undefined;
        this.idUsuario = '';
        this.seleccion = '';
        this.importeTotal = 0;

    }
}

import { BaseModel } from '../../_base/crud';
import { SocialNetworks } from './social-networks.model';

export class Solicitud extends BaseModel {
    id: number;
    idCliente: string;
    modelo: string;
    fechaSolicitud: string
    name: string;
    last_name_1:string;
    last_name_2:string;

    clear(): void {
        this.id = undefined;
        this.idCliente = '';
        this.fechaSolicitud = '';
        this.name = '';
        this.modelo = '';
        this.last_name_1 = "";
        this.last_name_2 = "";
    }
}

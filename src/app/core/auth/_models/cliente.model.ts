import { BaseModel } from '../../_base/crud';
export class Cliente extends BaseModel {
    id: number;
    CIFDNI: string;
    nombre: string;
    razonSocial: string;
    direccion: string;
    poblacion: string;
    codigoPostal: string;
    provincia: string;
    telefonoFijo: string;
    telefonoMovil: string;
    email: string;
    fechaAlta: string;
    observaciones: string;
    favorito: number;
    
    clear(): void {
        this.id = undefined;
        this.CIFDNI = '';
        this.nombre = '';
        this.razonSocial = '';
        this.direccion = '';
        this.poblacion = '';
        this.codigoPostal = '';
        this.provincia = '';
        this.telefonoFijo = '';
        this.telefonoMovil = '';
        this.email = '';
    }
}

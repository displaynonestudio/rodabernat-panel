import { BaseModel } from '../../_base/crud';

export class Referencia extends BaseModel {
    id: number;
    nombre: string;
    familia: number;
    subfamilia: number;
    codigo: string;
    tipo: string;
    largo: string;
    alto: string;
    profundo: string;
    relacionados: string;
    acabados: string;
    imagenes: string;
    clear(): void {
        this.id = undefined;
        this.familia = 0;
        this.subfamilia = 0;
        this.codigo = '';
        this.nombre = '';
        this.tipo = '';
        this.largo = '';
        this.alto = '';
        this.acabados = '';
        this.profundo = '';
    }
}

import { BaseModel } from '../../_base/crud';

export class Subfamilia extends BaseModel {
    id: number;
    nombre: string;
    codigo: string;
	familia: string;
	imagenes: string;
	
    clear(): void {
        this.id = undefined;
        this.nombre = '';
		this.codigo = '';
		this.familia = '';
        this.imagenes = '';
    }
}

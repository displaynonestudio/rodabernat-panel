export class MenuConfig {
	public defaults: any = {
		aside: {
			self: {},
			items: [
				{section: 'Usuarios y Roles'},
				{
					title: 'Usuarios',
					root: true,
					icon: 'flaticon-users-1',
					page: 'usuarios',
					translate: 'MENU.USERS',
					bullet: 'dot',
				},
				
				{
					title: 'Roles',
					root: true,
					icon: 'flaticon-network',
					page: 'roles',
					translate: 'MENU.ROLES',
					bullet: 'dot',
				},
				{section: 'Clientes'},
				{
					title: 'Clientes',
					root: true,
					icon: 'flaticon-customer',
					page: 'clientes',
					translate: 'MENU.CLIENTS',
					bullet: 'dot',
				},
				{section: 'Presupuestos y Albaranes'},
				{
					title: 'Presupuestos',
					root: true,
					icon: 'flaticon2-architecture-and-city',
					page: 'presupuestos',
					translate: 'MENU.PRESUPUESTOS',
					bullet: 'dot',
				},
				{
					title: 'Albaranes',
					root: true,
					icon: 'flaticon2-architecture-and-city',
					page: 'albaranes',
					translate: 'MENU.ALBARANES',
					bullet: 'dot',
				},
				{section: 'Partes'},
				{
					title: 'Partes',
					root: true,
					icon: 'flaticon2-architecture-and-city',
					page: 'partes',
					translate: 'MENU.PARTES',
					bullet: 'dot',
				},
				{
					title: 'Partes Sin Valorar',
					root: true,
					icon: 'flaticon2-architecture-and-city',
					page: 'partes-sin-val',
					translate: 'MENU.PARTESSINVAL',
					bullet: 'dot',
				},

				{section: 'Mantenimiento'},
				{
					title: 'Articulos',
					root: true,
					icon: 'flaticon2-architecture-and-city',
					page: 'articulos',
					bullet: 'dot',
				},
			]
		},
	};

	public get configs(): any {
		return this.defaults;
	}
}

import { Component, OnInit } from '@angular/core';
import * as $ from "jquery";
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from '../../services/api/api.service';
import { Familia } from '../../core/auth/_models/familia.model';
import { Subfamilia } from '../../core/auth/_models/subfamilia.model';
declare var $: any;
declare var jQuery: any;


@Component({
  selector: 'kt-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})

export class HomeComponent implements OnInit {
  familia: Familia;
  subfamilia: Subfamilia;
  imagenes = [];
  constructor(
    private activatedRoute: ActivatedRoute,
    private api: ApiService,
  ) { }

  ngOnInit() {

    this.activatedRoute.params.subscribe(params => {
      const codigoFamilia = params.familia;
      this.subfamilia = new Subfamilia();
      this.subfamilia.clear();
      this.familia = new Familia();
      this.familia.clear();

      if (codigoFamilia && codigoFamilia != "") {
        if (codigoFamilia && codigoFamilia != "") {
          this.api.readData('subfamilias','', 'codigo = "' + codigoFamilia +'"').subscribe(res => {
            if (res['data'].length > 0){
              this.subfamilia = Object.assign(this.subfamilia, res['data'][0]);
              if (this.subfamilia.imagenes != ""){
                var parseImagenes = JSON.parse(this.subfamilia.imagenes);
                for (var i in parseImagenes) {
                  //console.log(parseImagenes[i]);
                  this.imagenes.push([parseImagenes[i],window.location.protocol + "//" + window.location.hostname + "/api/assets/images/subfamilias/" + this.subfamilia.codigo + "/" + parseImagenes[i] ]);
                }
              }
            }else{
              this.api.readData('familias','', 'codigo = "' + codigoFamilia +'"').subscribe(res => {
                if (res['data'].length > 0){
                  this.familia = Object.assign(this.familia, res['data'][0]);
                  //console.log(this.familia);
                  if (this.familia.imagenes != ""){
                    var parseImagenes = JSON.parse(this.familia.imagenes);
                    for (var i in parseImagenes) {
                      //console.log(parseImagenes[i]);
                      this.imagenes.push([parseImagenes[i],window.location.protocol + "//" + window.location.hostname +"api/assets/images/familias/" + this.familia.codigo + "/" + parseImagenes[i] ]);
                    }
                  }
                }else{
                  console.log("redireccionar 2");
                }
              })
            }
          })
        }
      }else{
        console.log("redireccionar 1");
      }

      console.log(this.subfamilia);
      console.log(this.familia);
      console.log(this.imagenes);
		});

      $('.multiple-items').slick({
        infinite: true,
        arrows: false,
        dots: false,
        autoplay: true,
        autoplaySpeed: 1800,
        slidesToShow: 1,
        slidesToScroll: 1
    });
  }

}

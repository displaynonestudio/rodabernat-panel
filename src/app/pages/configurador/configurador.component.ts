import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from '../../services/api/api.service';
import { Familia } from '../../core/auth/_models/familia.model';
import { Subfamilia } from '../../core/auth/_models/subfamilia.model';
import { Referencia } from '../../core/auth/_models/referencia.model';
import { HttpClient } from '@angular/common/http';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
import { TipoReferencia } from '../../core/auth/_models/tipo-referencia.model';
import { resetFakeAsyncZone } from '@angular/core/testing';

declare var $: any;
declare var jQuery: any;

@Component({
  selector: 'kt-configurador',
  templateUrl: './configurador.component.html',
  styleUrls: ['./configurador.component.scss']
})
export class ConfiguradorComponent implements OnInit {
  familia: Familia;
  subfamilia: Subfamilia;
  public nombreFamilia: string = null;
  codigoFamilia: string;
  referenciasFamilia: Referencia[] = [];
  acabados = [];
  acabadosImg = [];
  referenciasFamiliaImg = [];
  referenciasRelacionadas = [];
  referenciasAcabados = [];
  acabadosSelect = [];
  acabadosRelSelect = [];
  refRelSelect = [];
  tiposReferencias = [];
  imagenes = [];
  contador = [];
  importeTotal = 0;

  AsistenteActualPrincipal = 1;
  AsistenteSituacion3 = [];
  
  mostrarPasoAsistente = [];

  principalSeleccionado = 0;
  mostrarModal = false;
  tituloModal = "";
  imagenModal = "";
  contenidoModal = "";
  tipoModal = 1;

  constructor(
    private activatedRoute: ActivatedRoute,
    private api: ApiService,
    private cdRef: ChangeDetectorRef,
    private sanitizer: DomSanitizer
  ) { }

  async ngOnInit() {

    this.mostrarPasoAsistente[1] = true;
    this.mostrarPasoAsistente[2] = false;
    this.mostrarPasoAsistente[3] = [];

    this.AsistenteSituacion3[0] = 0;
    this.AsistenteSituacion3[1] = 0;

    this.activatedRoute.params.subscribe(params => {
      this.codigoFamilia = params.familia;
    });

    this.subfamilia = new Subfamilia();
    this.subfamilia.clear();
    this.familia = new Familia();
    this.familia.clear();


    this.api.readData('tiposreferencias').subscribe(res => {
      for (let o of res['data']) {
        this.contador[o.id] = [];
        this.contador[3] = [];
        
        this.referenciasRelacionadas[o.id] = [];
        this.acabadosRelSelect[o.id] = [];
        if (o.id != 1){
          this.tiposReferencias.push(o);
          this.refRelSelect[o.id] = [];
          this.contador[3][o.id] = [];
          this.contador[3][o.id][0] = 0;
          this.contador[3][o.id][1] = 0;
          this.mostrarPasoAsistente[3][o.id] = [];
          this.mostrarPasoAsistente[3][o.id][0] = false;
          this.mostrarPasoAsistente[3][o.id][1] = false;
        }
      }
    });

    this.api.readData('acabados').subscribe(res => {
      for (let o of res['data']) {
        this.acabados[o.id] = o;
        if (o.imagenes != "") {
          var parseImagenes = JSON.parse(o.imagenes);
          for (var i in parseImagenes) {
            //console.log(parseImagenes[i]);
            this.acabadosImg[o.id] = [];
            this.acabadosImg[o.id].push(window.location.protocol + "//" + window.location.hostname + "/api/assets/images/acabados/" + o.grupo + o.codigoColor  + "/" + parseImagenes[i]);
          }
          
        }
      }
      //console.log(this.acabadosImg);
      //console.log(this.acabados);
    });

    if (this.codigoFamilia && this.codigoFamilia != "") {
      if (this.codigoFamilia && this.codigoFamilia != "") {
        this.api.readData('subfamilias', '', 'codigo = "' + this.codigoFamilia + '"').subscribe(res => {
          if (res['data'].length > 0) {
            this.subfamilia = Object.assign(this.subfamilia, res['data'][0]);
            //console.log(this.subfamilia);
            this.nombreFamilia = this.subfamilia.nombre;
            if (this.subfamilia.imagenes != "") {
              var parseImagenes = JSON.parse(this.subfamilia.imagenes);
              for (var i in parseImagenes) {
                //console.log(parseImagenes[i]);
                this.imagenes.push([parseImagenes[i], window.location.protocol + "//" + window.location.hostname + "/api/assets/images/subfamilias/" + this.subfamilia.codigo + "/" + parseImagenes[i]]);
              }
              //console.log(this.imagenes);
            }

            this.api.readData('referencias', '', 'subfamilia = "' + this.subfamilia.id + '"').subscribe(res => {
              for (let o of res['data']) {
                this.referenciasFamilia.push(o);
                this.contador[o.tipo][o.id] = [];
                this.contador[o.tipo][o.id] = 0;
                if (o.imagenes != "") {
                  var parseImagenes = JSON.parse(o.imagenes);
                  for (var i in parseImagenes) {

                    this.getImage("/api/assets/images/referencias/" + o.codigo + "/" + parseImagenes[i]).then((imagen) => {
                      this.referenciasFamiliaImg[o.id] = this.sanitizer.bypassSecurityTrustHtml(imagen);
                      //console.log(this.referenciasFamiliaImg[o.id]);
                      this.cdRef.detectChanges();
                    });
                  }
                }
                if (o.relacionados != "") {
                  var parseRelacionados = JSON.parse(o.relacionados);
                  for (var i in parseRelacionados.selected) {
                    this.api.readData('referencias', '', 'id = "' + parseRelacionados.selected[i] + '"').subscribe(resRel => {
                      //console.log(resRel['data']);  
                      for (let oRel of resRel['data']) {
                        //console.log(oRel.tipo);  
                        //console.log(o.id); 
                        if (!Array.isArray(this.referenciasRelacionadas[oRel.tipo][o.id] )) {
                          this.referenciasRelacionadas[oRel.tipo][o.id] = [];
                        }
                        
                        this.referenciasRelacionadas[oRel.tipo][o.id].push(oRel);
                        //console.log(oRel.imagenes);
                        if (oRel.imagenes != "") {
                          var parseImagenes = JSON.parse(oRel.imagenes);
                          for (var i in parseImagenes) {
                            //console.log("/api/assets/images/referencias/" + oRel.codigo + "/" + parseImagenes[i]);
                            this.getImage("/api/assets/images/referencias/" + oRel.codigo + "/" + parseImagenes[i]).then((imagen) => {
                              this.referenciasFamiliaImg[oRel.id] = this.sanitizer.bypassSecurityTrustHtml(imagen);
                              this.cdRef.detectChanges();
                            });
                          }
                        }

                        if (oRel.acabados != "") {
                          var parseAcabados = JSON.parse(oRel.acabados);
                          this.referenciasAcabados[oRel.id] = parseAcabados;
                        }

                        //console.log(parseAcabados);
                      }
                    });
                   
                  }
                }

                if (o.acabados != "") {
                  var parseAcabados = JSON.parse(o.acabados);
                  this.referenciasAcabados[o.id] = parseAcabados;
                }
              }
              //console.log(this.referenciasFamilia);
              this.cdRef.detectChanges();
            });

          } else {
            this.api.readData('familias', '', 'codigo = "' + this.codigoFamilia + '"').subscribe(res2 => {
              if (res2['data'].length > 0) {
                this.familia = Object.assign(this.familia, res2['data'][0]);
                //console.log(this.familia);

                //this.nombreFamilia = this.familia.nombre;
                if (this.familia.imagenes != "") {
                  var parseImagenes = JSON.parse(this.familia.imagenes);
                  for (var i in parseImagenes) {
                    //console.log(parseImagenes[i]);
                    this.imagenes.push([parseImagenes[i], window.location.protocol + "//" + window.location.hostname + "api/assets/images/familias/" + this.familia.codigo + "/" + parseImagenes[i]]);
                  }
                }
                this.api.readData('referencias', '', 'familia = "' + this.familia.id + '"').subscribe(res => {
                  for (let o of res['data']) {
                    this.referenciasFamilia.push(o);
                    this.contador[o.tipo][o.id] = [];
                    this.contador[o.tipo][o.id] = 0;
                    if (o.imagenes != "") {
                      var parseImagenes = JSON.parse(o.imagenes);
                      for (var i in parseImagenes) {
    
                        this.getImage("/api/assets/images/referencias/" + o.codigo + "/" + parseImagenes[i]).then((imagen) => {
                          this.referenciasFamiliaImg[o.id] = this.sanitizer.bypassSecurityTrustHtml(imagen);
                          this.cdRef.detectChanges();
                        });
                      }
                    }
                    
                  }
                  //console.log(this.referenciasFamilia);
                  this.cdRef.detectChanges();
                });
              } else {
                //console.log("redireccionar 2");
              }
            })
          }
          //console.log(this.referenciasRelacionadas);
        })
      }
    } else {
      //console.log("redireccionar 1");
    }

  }

  async getImage(url) {
    return await this.api.readSVG(url);
  }

  cargarFancy(idFancy){
    console.log(idFancy);
  }
  cerrarModal(){
    this.mostrarModal = false;
  }

  cargarModal(tipo:number,dato1="", dato2,dato3=""){

    console.log("tipo: " + tipo);
    console.log("dato1: " + dato1);
    
    console.log("dato3: " + dato3);

    this.mostrarModal = true;
    this.tituloModal = dato1;
    this.imagenModal = dato2;
    this.contenidoModal = dato3;
    this.tipoModal = tipo;

    this.cdRef.detectChanges();

  }

  incrDecrSelecc(id, tipo: number,subtipo:number,numero:number) {
    console.log("id: " + id);
    console.log("tipo: " + tipo);
    //console.log(this.referenciasAcabados[id].selected);
    if (tipo == 1){

      for (var key in this.contador[tipo]) {
        this.acabadosSelect = [];
        if (key == id) {
          this.contador[tipo][key] = this.contador[tipo][key] + numero;
          if (this.contador[tipo][key] < 0){
            this.contador[tipo][key] = 0;
            this.principalSeleccionado = 0;
          }
          if (this.contador[tipo][key] > 0){
            this.principalSeleccionado = 1;
          }
        } else {
          this.contador[tipo][key] = 0;
        }
      }
      for (let acabado of this.referenciasAcabados[id].selected){
        this.acabadosSelect.push(acabado);
      }
      for (let tipoReferencia of this.tiposReferencias) {
          if (this.referenciasRelacionadas[tipoReferencia.id][id]){
            this.refRelSelect[tipoReferencia.id] = this.referenciasRelacionadas[tipoReferencia.id][id];
          }
      }

    }else if(tipo == 2){
      this.contador[tipo] = id;
    }


    this.cdRef.detectChanges();

  }

  selectSubTipo(id,tipo){
    //console.log(id);
    //console.log(tipo);
    //this.contador[3][tipo][0]=id;

    this.acabadosRelSelect[tipo] = [];
    for (let acabado of this.referenciasAcabados[id].selected){
      this.acabadosRelSelect[tipo].push(acabado);
    }
      
  }

  selectAcabadoSubTipo(id,tipo){
    //console.log(id);
    //console.log(tipo);
    this.contador[3][tipo][1]=id;
      
  }

  situacionAsistente(accion:number){

    console.log(this.mostrarPasoAsistente);

    let referenciasConValores = [];

    let actualPrincipal = this.AsistenteActualPrincipal;

    if (accion == 1){
      if (this.AsistenteActualPrincipal<3){
        this.AsistenteActualPrincipal ++;
      }
    }else{
      this.AsistenteActualPrincipal --;
    }

    if (this.AsistenteActualPrincipal== 3){
      //console.log("entra en else");
        this.mostrarPasoAsistente[2] = false;


      var contador = 0;
      for (let tipoReferencia of this.tiposReferencias) {            
        if (this.referenciasRelacionadas[tipoReferencia.id] !=""){
          contador++;
          referenciasConValores[contador] = tipoReferencia.id;
          
        }  
      }

      if (this.AsistenteSituacion3[0] == 0 && this.AsistenteSituacion3[1] == 0){
      
        console.log("entra en else 0");
        this.mostrarPasoAsistente[3][referenciasConValores[1]][0] = true;
        this.AsistenteSituacion3[0] = 1;
      
      }else if(this.AsistenteSituacion3[0] > 0 && this.AsistenteSituacion3[1] == 0){
      
        console.log("entra en else 1");
        this.mostrarPasoAsistente[3][referenciasConValores[this.AsistenteSituacion3[0]]][0] = false;
        this.mostrarPasoAsistente[3][referenciasConValores[this.AsistenteSituacion3[0]]][1] = true;
        this.AsistenteSituacion3[1] = 1;

      }else if(this.AsistenteSituacion3[0] > 0 && this.AsistenteSituacion3[1] == 1){
        this.mostrarPasoAsistente[3][referenciasConValores[this.AsistenteSituacion3[0]]][0] = false;
        this.mostrarPasoAsistente[3][referenciasConValores[this.AsistenteSituacion3[0]]][1] = false;

        console.log("entra en else 2");
        this.AsistenteSituacion3[0] ++;
        this.AsistenteSituacion3[0] = 0;
        this.mostrarPasoAsistente[3][referenciasConValores[this.AsistenteSituacion3[0]]][0] = true;
        this.mostrarPasoAsistente[3][referenciasConValores[this.AsistenteSituacion3[0]]][1] = false;
        
      }
      

      /*
      if (this.AsistenteSituacion3[0] == 0 ){
        if (this.mostrarPasoAsistente[3][tipoReferencia.id][0]){
          this.mostrarPasoAsistente[3][tipoReferencia.id][0] = false;
          this.mostrarPasoAsistente[3][tipoReferencia.id][1] = true;
        }else{
          this.mostrarPasoAsistente[3][tipoReferencia.id][0] = true;
        }
      }
      */






    }else{
      console.log("entra en if");
      this.mostrarPasoAsistente[actualPrincipal] = false;
      this.mostrarPasoAsistente[this.AsistenteActualPrincipal] = true;
    }

    console.log(this.mostrarPasoAsistente);
    window.scroll(0,0);
  }


}


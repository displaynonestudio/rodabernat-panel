import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteArticuloComponent } from './delete-articulo.component';

describe('DeleteArticuloComponent', () => {
  let component: DeleteArticuloComponent;
  let fixture: ComponentFixture<DeleteArticuloComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeleteArticuloComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteArticuloComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

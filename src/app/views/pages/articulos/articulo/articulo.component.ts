import { Component, OnInit, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { ActivatedRoute, Router } from '@angular/router';
// Services
import { ApiService } from '../../../../services/api/api.service';
import { HeaderService } from '../../../../services/header/header.service';
import { FooterService } from '../../../../services/footer/footer.service';
// Models
import { Articulo } from '../../../../core/auth/_models/articulo.model';
import { Observable } from 'rxjs';
import { SubheaderService } from '../../../../core/_base/layout';
import {DomSanitizer} from '@angular/platform-browser';
@Component({
	selector: 'kt-articulo',
	templateUrl: './articulo.component.html',
	styleUrls: ['./articulo.component.scss']
})
export class ArticuloComponent implements OnInit, OnDestroy {

	articulo: Articulo;
	articuloId: Observable<number>;
	articuloForm: FormGroup;
	hasFormErrors: boolean = false;
	selectedTab = 0;
	loadingFile: boolean = false;


	constructor(
		public _DomSanitizer: DomSanitizer,
		private activatedRoute: ActivatedRoute,
		private router: Router,
		private fb: FormBuilder,
		private subheaderService: SubheaderService,
		private api: ApiService,
		private header: HeaderService,
		private footer: FooterService,
		private cdRef: ChangeDetectorRef) {
		this.header.hide();
		this.footer.hide();
	}



	ngOnInit() {
		this.activatedRoute.params.subscribe(params => {
			const id = params.id;
			this.articulo = new Articulo();
			this.articulo.clear();
			if (id && id > 0) {
				this.api.readData('articulos', '', 'id = ' + id).subscribe(res => {
					this.articulo = Object.assign(this.articulo, res['data'][0]);
					console.log(this.articulo);

					this.initArticulo();
				})
			} else {
				this.initArticulo();
			}
		});

	}

	initArticulo() {
		this.createForm();
		if (!this.articulo.id) {
			this.subheaderService.setTitle('Crear articulo');
			this.subheaderService.setBreadcrumbs([
				{ title: 'Articulos', page: `articulos` },
				{ title: 'Crear articulo', page: `articulos/new` }
			]);
			return;
		}
		this.subheaderService.setTitle('Editar Familia');
		this.subheaderService.setBreadcrumbs([
			{ title: 'Articulos', page: `articulos` },
			{ title: 'Editar articulo', page: `articulos`, queryParams: { id: this.articulo.id } }
		]);
	}

	createForm() {
		this.articuloForm = this.fb.group({
      decripcion: [this.articulo.descripcion],
      venta: [this.articulo.venta],
      unidad: [this.articulo.unidad],
      sd: [this.articulo.sd],
      sr: [this.articulo.sr],
      tipo: [this.articulo.tipo],
      stock: [this.articulo.stock],
      iva: [this.articulo.coste],
      produccion: [this.articulo.produccion],
		});

	}

	getComponentTitle() {
		let result = 'Crear articulo';
		if (!this.articulo || !this.articulo.id) {
			return result;
		}

		result = `Editar articulo - ${this.articulo.descripcion}`;
		return result;
	}

	saveArticulo() {
		const newArticulo = Object.assign(this.articulo, this.articuloForm.value);
		if (!newArticulo.id) {
			this.api.createData('articulos', newArticulo).subscribe(res => {
				console.log(res);
				if (res) {
					this.router.navigate(['/panel/articulos']);
				}
			});
		} else {
			this.api.updateData('articulos', newArticulo).subscribe(res => {
				console.log(res);
				if (res) {
					this.router.navigate(['/panel/articulos']);
				}
			});
		}
		/*
			
		*/
	}

	updateUrl(event) {
		//event.target.src = '../../../../../assets/img/users/default.jpg';
	}

	ngOnDestroy(): void {
		this.header.toggle();
		this.footer.toggle();
	}

}

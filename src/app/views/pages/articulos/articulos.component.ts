// Angular
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
// Material
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';
// Services
import { ApiService } from '../../../services/api/api.service';
import { HeaderService } from '../../../services/header/header.service';
import { FooterService } from '../../../services/footer/footer.service';
// Models
import { Articulo } from '../../../core/auth/_models/articulo.model';
import { SubheaderService } from '../../../core/_base/layout';
// RXJS
import { Subscription } from 'rxjs';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { DeleteArticuloComponent } from './articulo/delete-articulo/delete-articulo.component';

@Component({
  selector: 'kt-articulos',
  templateUrl: './articulos.component.html',
  styleUrls: ['./articulos.component.scss']
})
export class ArticulosComponent implements OnInit {

  articulos: Articulo[] = [];
  dataSource;
  displayedColumns: string[] = ['select', 'id', 'descripcion', 'opciones'];

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  selection = new SelectionModel<Articulo>(true, []);

  constructor(
    private api: ApiService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private subheaderService: SubheaderService,
    private matDialog: MatDialog,
    private header: HeaderService,
    private footer: FooterService) {
    this.header.hide();
    this.footer.hide();
  }

  ngOnInit() {

    this.subheaderService.setTitle('Articulos');

    this.api.readData('articulos').subscribe(res => {
      for (let o of res['data']) {
        this.articulos.push(o);
      }
      this.dataSource = new MatTableDataSource(this.articulos);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;

    });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  isAllSelected(): boolean {
    const numSelected = this.selection.selected.length;
    const numRows = this.articulos.length;
    return numSelected === numRows;
  }

  masterToggle() {
    if (this.selection.selected.length === this.articulos.length) {
      this.selection.clear();
    } else {
      this.articulos.forEach(row => this.selection.select(row));
    }
  }

  fetchClients() {
    const messages = [];
    this.selection.selected.forEach(elem => {
      messages.push({
        text: `${elem.descripcion}`,
        id: elem.id.toString(),
        status: elem.descripcion
      });
    });
    console.log(messages);

  }

  editClient(id: number) {
    this.router.navigate(['../articulos/', id], { relativeTo: this.activatedRoute });
  }

  deleteClient(id: number) {
    let client = this.articulos.find(i => i.id === id);
    const dialogConfig: MatDialogConfig = {
      autoFocus: true,
      maxWidth: '400px',
      data: {
        id: client.id,
        name: client.descripcion
      }
    }
    const dialogRef = this.matDialog.open(DeleteArticuloComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(res => {
      if (res) {
        this.api.deleteData('articulos', res).subscribe(res => {
          if (res) {
            this.dataSource.data = this.dataSource.data.filter(obj => obj.id !== id);
          }
        });
      }
    });
  }

  recargarArticulos() {
    this.api.readData('articulos').subscribe(res => {
      this.articulos = [];
      for (let o of res['data']) {
        this.articulos.push(o);
      }
      this.dataSource = new MatTableDataSource(this.articulos);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ParteSinValComponent } from './parte-sin-val.component';

describe('ParteSinValComponent', () => {
  let component: ParteSinValComponent;
  let fixture: ComponentFixture<ParteSinValComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParteSinValComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParteSinValComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

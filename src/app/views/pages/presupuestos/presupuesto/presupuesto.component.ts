import { Component, OnInit, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { ActivatedRoute, Router } from '@angular/router';
// Services
import { ApiService } from '../../../../services/api/api.service';
import { HeaderService } from '../../../../services/header/header.service';
import { FooterService } from '../../../../services/footer/footer.service';
// Models
import { Presupuesto } from '../../../../core/auth/_models/presupuesto.model';
import { Observable } from 'rxjs';
import { SubheaderService } from '../../../../core/_base/layout';
import {DomSanitizer} from '@angular/platform-browser';
@Component({
	selector: 'kt-presupuesto',
	templateUrl: './presupuesto.component.html',
	styleUrls: ['./presupuesto.component.scss']
})
export class PresupuestoComponent implements OnInit, OnDestroy {

	presupuesto: Presupuesto;
	presupuestoId: Observable<number>;
	presupuestoForm: FormGroup;
	hasFormErrors: boolean = false;
	selectedTab = 0;
	loadingFile: boolean = false;


	constructor(
		public _DomSanitizer: DomSanitizer,
		private activatedRoute: ActivatedRoute,
		private router: Router,
		private fb: FormBuilder,
		private subheaderService: SubheaderService,
		private api: ApiService,
		private header: HeaderService,
		private footer: FooterService,
		private cdRef: ChangeDetectorRef) {
		this.header.hide();
		this.footer.hide();
	}



	ngOnInit() {
		this.activatedRoute.params.subscribe(params => {
			const id = params.id;
			this.presupuesto = new Presupuesto();
			this.presupuesto.clear();
			if (id && id > 0) {
				this.api.readData('Presupuestos', '', 'id = ' + id).subscribe(res => {
					this.presupuesto = Object.assign(this.presupuesto, res['data'][0]);


					this.initpresupuesto();
				})
			} else {
				this.initpresupuesto();
			}
		});

	}

	initpresupuesto() {
		this.createForm();
		if (!this.presupuesto.id) {
			this.subheaderService.setTitle('Crear presupuesto');
			this.subheaderService.setBreadcrumbs([
				{ title: 'Presupuestos', page: `Presupuestos` },
				{ title: 'Crear presupuesto', page: `Presupuestos/new` }
			]);
			return;
		}
		this.subheaderService.setTitle('Editar Familia');
		this.subheaderService.setBreadcrumbs([
			{ title: 'Presupuestos', page: `Presupuestos` },
			{ title: 'Editar presupuesto', page: `Presupuestos`, queryParams: { id: this.presupuesto.id } }
		]);
	}

	createForm() {
		this.presupuestoForm = this.fb.group({
      fecha: [this.presupuesto.fecha],
      validez: [this.presupuesto.validez],
      cliente: [this.presupuesto.cliente],
      descripcion: [this.presupuesto.datos],
      direccionTrabajo: [this.presupuesto.direccionTrabajo],
      datos: [this.presupuesto.datos],

		});

	}

	getComponentTitle() {
		let result = 'Crear presupuesto';
		if (!this.presupuesto || !this.presupuesto.id) {
			return result;
		}

		result = `Editar presupuesto - ${this.presupuesto.id}`;
		return result;
	}

	savepresupuesto() {
		const newpresupuesto = Object.assign(this.presupuesto, this.presupuestoForm.value);
		if (!newpresupuesto.id) {
			this.api.createData('Presupuestos', newpresupuesto).subscribe(res => {
				console.log(res);
				if (res) {
					this.router.navigate(['/panel/Presupuestos']);
				}
			});
		} else {
			this.api.updateData('Presupuestos', newpresupuesto).subscribe(res => {
				console.log(res);
				if (res) {
					this.router.navigate(['/panel/Presupuestos']);
				}
			});
		}
		/*
			
		*/
	}

	updateUrl(event) {
		//event.target.src = '../../../../../assets/img/users/default.jpg';
	}

	ngOnDestroy(): void {
		this.header.toggle();
		this.footer.toggle();
	}

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeletePresupuestoComponent } from './delete-presupuesto.component';

describe('DeletePresupuestoComponent', () => {
  let component: DeletePresupuestoComponent;
  let fixture: ComponentFixture<DeletePresupuestoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeletePresupuestoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeletePresupuestoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

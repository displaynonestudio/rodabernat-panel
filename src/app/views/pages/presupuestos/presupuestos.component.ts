// Angular
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
// Material
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';
// Services
import { ApiService } from '../../../services/api/api.service';
import { HeaderService } from '../../../services/header/header.service';
import { FooterService } from '../../../services/footer/footer.service';
// Models
import { Presupuesto } from '../../../core/auth/_models/presupuesto.model';
import { SubheaderService } from '../../../core/_base/layout';
// RXJS
import { Subscription } from 'rxjs';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { DeletePresupuestoComponent } from './presupuesto/delete-presupuesto/delete-presupuesto.component';

@Component({
  selector: 'kt-presupuestos',
  templateUrl: './presupuestos.component.html',
  styleUrls: ['./presupuestos.component.scss']
})
export class PresupuestosComponent implements OnInit {

  presupuestos: Presupuesto[] = [];
  clientes = [];
  dataSource;
  displayedColumns: string[] = ['select', 'id', 'fecha','validez', 'cliente', 'opciones'];

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  selection = new SelectionModel<Presupuesto>(true, []);

  constructor(
    private api: ApiService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private subheaderService: SubheaderService,
    private matDialog: MatDialog,
    private header: HeaderService,
    private footer: FooterService) {
    this.header.hide();
    this.footer.hide();
  }

  ngOnInit() {

    this.subheaderService.setTitle('Presupuestos');

    this.api.readData('clientes').subscribe(res => {
      for (let o of res['data']) {
          this.clientes[o.id] = o;
      }
      //console.log(this.clientes);
    });
    
    this.api.readData('presupuestos').subscribe(res => {
      for (let o of res['data']) {
        this.presupuestos.push(o);
      }
      this.dataSource = new MatTableDataSource(this.presupuestos);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;

    });

  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  isAllSelected(): boolean {
    const numSelected = this.selection.selected.length;
    const numRows = this.presupuestos.length;
    return numSelected === numRows;
  }

  masterToggle() {
    if (this.selection.selected.length === this.presupuestos.length) {
      this.selection.clear();
    } else {
      this.presupuestos.forEach(row => this.selection.select(row));
    }
  }

  fetchPresupuestos() {
    const messages = [];
    this.selection.selected.forEach(elem => {
      messages.push({
        text: `${elem.id}`,
        id: elem.id.toString(),
        status: elem.id
      });
    });
    console.log(messages);

  }

  editPresupuesto(id: number) {
    this.router.navigate(['../presupuestos/', id], { relativeTo: this.activatedRoute });
  }

  deletePresupuesto(id: number) {
    let presupuesto = this.presupuestos.find(i => i.id === id);
    const dialogConfig: MatDialogConfig = {
      autoFocus: true,
      maxWidth: '400px',
      data: {
        id: presupuesto.id,
        name: presupuesto.cliente
      }
    }
    const dialogRef = this.matDialog.open(DeletePresupuestoComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(res => {
      if (res) {
        this.api.deleteData('presupuestos', res).subscribe(res => {
          if (res) {
            this.dataSource.data = this.dataSource.data.filter(obj => obj.id !== id);
          }
        });
      }
    });
  }

  recargarPresupuestos() {
    this.api.readData('presupuestos').subscribe(res => {
      this.presupuestos = [];
      for (let o of res['data']) {
        this.presupuestos.push(o);
      }
      this.dataSource = new MatTableDataSource(this.presupuestos);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }
}

// Angular
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';
// Partials
import { PartialsModule } from '../partials/partials.module';
// Pages
import { CoreModule } from '../../core/core.module';

import { ClientesComponent } from './clientes/clientes.component';
import { PedidosComponent } from './pedidos/pedidos.component';
import { UsuariosComponent } from './usuarios/usuarios.component';
import { UsuarioComponent } from './usuarios/usuario/usuario.component';
import { DeleteUserComponent } from './usuarios/usuario/delete-user/delete-user.component';

import { ClienteComponent } from './clientes/cliente/cliente.component';
import { PedidoComponent } from './pedidos/pedido/pedido.component';


import { MatTableModule } from '@angular/material/table';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';

import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatMenuModule } from '@angular/material/menu';
import { MatTabsModule } from '@angular/material/tabs';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatDialogModule } from '@angular/material/dialog';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatRadioModule } from '@angular/material/radio';
import { MAT_DATE_LOCALE } from '@angular/material';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { DeleteClienteComponent } from './clientes/cliente/delete-cliente/delete-cliente.component';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { DeletePedidoComponent } from './pedidos/pedido/delete-pedido/delete-pedido.component';
import { SelectAutocompleteModule } from 'mat-select-autocomplete';
import { RolesComponent } from './roles/roles.component';
import { RolComponent } from './roles/rol/rol.component';
import { DeleteRolComponent } from './roles/rol/delete-rol/delete-rol.component';
import { PresupuestosComponent } from './presupuestos/presupuestos.component';
import { AlbaranesComponent } from './albaranes/albaranes.component';
import { PartesComponent } from './partes/partes.component';
import { PartesSinValComponent } from './partes-sin-val/partes-sin-val.component';
import { PresupuestoComponent } from './presupuestos/presupuesto/presupuesto.component';
import { AlbaranComponent } from './albaranes/albaran/albaran.component';
import { ParteComponent } from './partes/parte/parte.component';
import { ParteSinValComponent } from './partes-sin-val/parte-sin-val/parte-sin-val.component';
import { DeletePresupuestoComponent } from './presupuestos/presupuesto/delete-presupuesto/delete-presupuesto.component';
import { ArticulosComponent } from './articulos/articulos.component';
import { ArticuloComponent } from './articulos/articulo/articulo.component';
import { DeleteArticuloComponent } from './articulos/articulo/delete-articulo/delete-articulo.component';


@NgModule({
	declarations: [
		UsuariosComponent,
		ClientesComponent,
		PedidosComponent,
		UsuarioComponent,
		ClienteComponent,
		PedidoComponent,
		DeleteUserComponent,
		DeleteClienteComponent,
		DeletePedidoComponent,
		RolesComponent,
		RolComponent,
		DeleteRolComponent,
		PresupuestosComponent,
		AlbaranesComponent,
		PartesComponent,
		PartesSinValComponent,
		PresupuestoComponent,
		AlbaranComponent,
		ParteComponent,
		ParteSinValComponent,
		DeletePresupuestoComponent,
		ArticulosComponent,
		ArticuloComponent,
		DeleteArticuloComponent,
	],
	exports: [],
	imports: [
		CommonModule,
		HttpClientModule,
		FormsModule,
		CoreModule,
		PartialsModule,
		MatTableModule,
		MatFormFieldModule,
		MatInputModule,
		RouterModule,
		MatIconModule,
		MatButtonModule,
		MatPaginatorModule,
		MatSortModule,
		MatCheckboxModule,
		MatMenuModule,
		MatTabsModule,
		MatProgressBarModule,
		MatDialogModule,
		MatDatepickerModule,
		MatRadioModule,
		MatProgressSpinnerModule,
		ReactiveFormsModule,
		MatSelectModule,
		SelectAutocompleteModule,
	],
	providers: [{ provide: MAT_DATE_LOCALE, useValue: 'es-ES' }],
	entryComponents: [DeleteUserComponent, DeleteClienteComponent]

})
export class PagesModule {
}

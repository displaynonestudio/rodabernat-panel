import { Component, OnInit, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { ActivatedRoute, Router } from '@angular/router';
// Services
import { ApiService } from '../../../../services/api/api.service';
import { HeaderService } from '../../../../services/header/header.service';
import { FooterService } from '../../../../services/footer/footer.service';
// Models
import { Cliente } from '../../../../core/auth/_models/cliente.model';
import { Observable } from 'rxjs';
import { SubheaderService } from '../../../../core/_base/layout';
import {DomSanitizer} from '@angular/platform-browser';
@Component({
	selector: 'kt-cliente',
	templateUrl: './cliente.component.html',
	styleUrls: ['./cliente.component.scss']
})
export class ClienteComponent implements OnInit, OnDestroy {

	cliente: Cliente;
	clienteId: Observable<number>;
	clienteForm: FormGroup;
	hasFormErrors: boolean = false;
	selectedTab = 0;
	loadingFile: boolean = false;


	constructor(
		public _DomSanitizer: DomSanitizer,
		private activatedRoute: ActivatedRoute,
		private router: Router,
		private fb: FormBuilder,
		private subheaderService: SubheaderService,
		private api: ApiService,
		private header: HeaderService,
		private footer: FooterService,
		private cdRef: ChangeDetectorRef) {
		this.header.hide();
		this.footer.hide();
	}



	ngOnInit() {
		this.activatedRoute.params.subscribe(params => {
			const id = params.id;
			this.cliente = new Cliente();
			this.cliente.clear();
			if (id && id > 0) {
				this.api.readData('clientes', '', 'id = ' + id).subscribe(res => {
					this.cliente = Object.assign(this.cliente, res['data'][0]);
					console.log(this.cliente);

					this.initCliente();
				})
			} else {
				this.initCliente();
			}
		});

	}

	initCliente() {
		this.createForm();
		if (!this.cliente.id) {
			this.subheaderService.setTitle('Crear cliente');
			this.subheaderService.setBreadcrumbs([
				{ title: 'Clientes', page: `clientes` },
				{ title: 'Crear cliente', page: `clientes/new` }
			]);
			return;
		}
		this.subheaderService.setTitle('Editar Familia');
		this.subheaderService.setBreadcrumbs([
			{ title: 'Clientes', page: `clientes` },
			{ title: 'Editar cliente', page: `clientes`, queryParams: { id: this.cliente.id } }
		]);
	}

	createForm() {
		this.clienteForm = this.fb.group({
      CIFDNI: [this.cliente.CIFDNI],
      nombre: [this.cliente.nombre],
      razonSocial: [this.cliente.razonSocial],
      direccion: [this.cliente.direccion],
      poblacion: [this.cliente.poblacion],
      codigoPostal: [this.cliente.codigoPostal],
      provincia: [this.cliente.provincia],
      telefonoFijo: [this.cliente.telefonoFijo],
      telefonoMovil: [this.cliente.telefonoMovil],
      email: [this.cliente.email],
      fechaAlta: [this.cliente.fechaAlta],
      observaciones: [this.cliente.observaciones],
      favorito: [this.cliente.favorito],

		});

	}

	getComponentTitle() {
		let result = 'Crear cliente';
		if (!this.cliente || !this.cliente.id) {
			return result;
		}

		result = `Editar cliente - ${this.cliente.nombre}`;
		return result;
	}

	saveCliente() {
		const newCliente = Object.assign(this.cliente, this.clienteForm.value);
		if (!newCliente.id) {
			this.api.createData('clientes', newCliente).subscribe(res => {
				console.log(res);
				if (res) {
					this.router.navigate(['/panel/clientes']);
				}
			});
		} else {
			this.api.updateData('clientes', newCliente).subscribe(res => {
				console.log(res);
				if (res) {
					this.router.navigate(['/panel/clientes']);
				}
			});
		}
		/*
			
		*/
	}

	updateUrl(event) {
		//event.target.src = '../../../../../assets/img/users/default.jpg';
	}

	ngOnDestroy(): void {
		this.header.toggle();
		this.footer.toggle();
	}

}

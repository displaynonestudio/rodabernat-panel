// Angular
import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
// Material
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';
// Services
import { ApiService } from '../../../services/api/api.service';
import { HeaderService } from '../../../services/header/header.service';
import { FooterService } from '../../../services/footer/footer.service';
// Models
import { Rol } from '../../../core/auth/_models/rol.model';
import { SubheaderService } from '../../../core/_base/layout';
// RXJS
import { Subscription, from } from 'rxjs';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { DeleteRolComponent } from './rol/delete-rol/delete-rol.component';

@Component({
	selector: 'kt-roles',
	templateUrl: './roles.component.html',
	styleUrls: ['./roles.component.scss']
})
export class RolesComponent implements OnInit, OnDestroy {

	roles: Rol[] = [];
	dataSource;
	displayedColumns: string[] = ['select', 'id', 'rol', 'opciones'];

	@ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
	@ViewChild(MatSort, { static: true }) sort: MatSort;

	selection = new SelectionModel<Rol>(true, []);

	constructor(
		private api: ApiService,
		private activatedRoute: ActivatedRoute,
		private router: Router,
		private subheaderService: SubheaderService,
		private matDialog: MatDialog,
		private header: HeaderService,
		private footer: FooterService) {
		this.header.hide();
		this.footer.hide();
	}

	ngOnInit() {

		this.subheaderService.setTitle('Roles');

		this.api.readData('roles').subscribe(res => {
			for (let o of res['data']) {
				this.roles.push(o);
			}
			this.dataSource = new MatTableDataSource(this.roles);
			this.dataSource.paginator = this.paginator;
			this.dataSource.sort = this.sort;

		});
	}

	applyFilter(event: Event) {
		const filterValue = (event.target as HTMLInputElement).value;
		this.dataSource.filter = filterValue.trim().toLowerCase();

		if (this.dataSource.paginator) {
			this.dataSource.paginator.firstPage();
		}
	}

	isAllSelected(): boolean {
		const numSelected = this.selection.selected.length;
		const numRows = this.roles.length;
		return numSelected === numRows;
	}

	masterToggle() {
		if (this.selection.selected.length === this.roles.length) {
			this.selection.clear();
		} else {
			this.roles.forEach(row => this.selection.select(row));
		}
	}

	fetchRols() {
		const messages = [];
		this.selection.selected.forEach(elem => {
			messages.push({
				text: `${elem.rol}`,
				id: elem.id.toString(),
				status: elem.rol
			});
		});
		console.log(messages);

	}

	editRol(id: number) {
		this.router.navigate(['../roles/', id], { relativeTo: this.activatedRoute });
	}

	deleteRol(id: number) {
		let rol = this.roles.find(i => i.id === id);
		const dialogConfig: MatDialogConfig = {
			autoFocus: true,
			maxWidth: '400px',
			data: {
				id: rol.id,
				nombre: rol.rol,
			}
		}
		const dialogRef = this.matDialog.open(DeleteRolComponent, dialogConfig);
		dialogRef.afterClosed().subscribe(res => {
			if (res) {
				this.api.deleteData('roles', res).subscribe(res => {
					if (res) {
						this.dataSource.data = this.dataSource.data.filter(obj => obj.id !== id);
					}
				});
			}
		});
	}

	recargarRoles() {
		this.api.readData('roles').subscribe(res => {
			this.roles = [];
			for (let o of res['data']) {
				this.roles.push(o);
			}
			this.dataSource = new MatTableDataSource(this.roles);
			this.dataSource.paginator = this.paginator;
			this.dataSource.sort = this.sort;
		});
	}

	ngOnDestroy(): void {
		this.header.toggle();
	}
}

import { Component, OnInit, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { ActivatedRoute, Router } from '@angular/router';
// Services
import { ApiService } from '../../../../services/api/api.service';
import { HeaderService } from '../../../../services/header/header.service';
import { FooterService } from '../../../../services/footer/footer.service';
// Models
import { Rol } from '../../../../core/auth/_models/rol.model';
import { Observable } from 'rxjs';
import { SubheaderService } from '../../../../core/_base/layout';
import {DomSanitizer} from '@angular/platform-browser';
@Component({
	selector: 'kt-rol',
	templateUrl: './rol.component.html',
	styleUrls: ['./rol.component.scss']
})
export class RolComponent implements OnInit, OnDestroy {

	rol: Rol;
	rolId: Observable<number>;
	rolForm: FormGroup;
	hasFormErrors: boolean = false;
	selectedTab = 0;
	loadingFile: boolean = false;
	imagenesRol = [];

	constructor(
		public _DomSanitizer: DomSanitizer,
		private activatedRoute: ActivatedRoute,
		private router: Router,
		private fb: FormBuilder,
		private subheaderService: SubheaderService,
		private api: ApiService,
		private header: HeaderService,
		private footer: FooterService,
		private cdRef: ChangeDetectorRef) {
		this.header.hide();
		this.footer.hide();
	}



	ngOnInit() {
		this.activatedRoute.params.subscribe(params => {
			const id = params.id;
			this.rol = new Rol();
			this.rol.clear();
			if (id && id > 0) {
				this.api.readData('roles', '', 'id = ' + id).subscribe(res => {
					this.rol = Object.assign(this.rol, res['data'][0]);
					console.log(this.rol);

					this.initRol();
				})
			} else {
				this.initRol();
			}
		});

	}

	initRol() {
		this.createForm();
		if (!this.rol.id) {
			this.subheaderService.setTitle('Crear rol');
			this.subheaderService.setBreadcrumbs([
				{ title: 'Roles', page: `roles` },
				{ title: 'Crear rol', page: `roles/new` }
			]);
			return;
		}
		this.subheaderService.setTitle('Editar Familia');
		this.subheaderService.setBreadcrumbs([
			{ title: 'Roles', page: `roles` },
			{ title: 'Editar rol', page: `roles`, queryParams: { id: this.rol.id } }
		]);
	}

	createForm() {
		this.rolForm = this.fb.group({
			rol: [this.rol.rol],
		});

		console.log(this.rolForm);
	}

	getComponentTitle() {
		let result = 'Crear rol';
		if (!this.rol || !this.rol.id) {
			return result;
		}

		result = `Editar rol - ${this.rol.rol}`;
		return result;
	}

	saveRol() {
		const newRol = Object.assign(this.rol, this.rolForm.value);
		newRol["imagenes"] = this.imagenesRol;
		if (!newRol.id) {
			this.api.createData('roles', newRol).subscribe(res => {
				console.log(res);
				if (res) {
					this.router.navigate(['/panel/roles']);
				}
			});
		} else {
			this.api.updateData('roles', newRol).subscribe(res => {
				console.log(res);
				if (res) {
					this.router.navigate(['/panel/roles']);
				}
			});
		}
		/*
			
		*/
	}
	borrarImagen(imagen) {
		this.imagenesRol = this.imagenesRol.filter(obj => obj !== imagen);
		this.cdRef.detectChanges();
	}
	preview(files) {

		if (files.length === 0) {
			return;
		}
		//console.log(files);

		for (var _i = 0; _i < files.length; _i++) {
			this.cargarImagenes(files[_i]);
		}
	}
	cargarImagenes(imagen) {
		var mimeType = imagen.type;
		if (mimeType.match(/image\/*/) == null) {
			return;
		}
		//console.log(imagen);
		var reader = new FileReader();
		//this.referenciaForm['pic'] = files;
		reader.readAsDataURL(imagen);
		reader.onload = (_event) => {
			var resultado =reader.result;
			this.imagenesRol.push([imagen.name,this._DomSanitizer.bypassSecurityTrustUrl(resultado.toString())]);
			this.cdRef.detectChanges();
		}

	}

	updateUrl(event) {
		//event.target.src = '../../../../../assets/img/users/default.jpg';
	}

	ngOnDestroy(): void {
		this.header.toggle();
		this.footer.toggle();
	}

}

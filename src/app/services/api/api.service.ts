import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
	providedIn: 'root'
})
export class ApiService {

	URL: string = 'http://localhost/api/datatables/';

	constructor(protected http: HttpClient) { }

	readData(table, fields: string = '', filter: string = '', order: string = '') {

		const userToken = localStorage.getItem(environment.authTokenKey);
		const httpHeaders = new HttpHeaders({
			'Authorization': 'Bearer ' + userToken
		});

		return this.http.post(this.URL + `listado.php`, { table: table, fields: fields, filter: filter, order: order }, { headers: httpHeaders });
	}

	deleteData(table: string, data: string) {

		const userToken = localStorage.getItem(environment.authTokenKey);
		const httpHeaders = new HttpHeaders({
			'Authorization': 'Bearer ' + userToken
		});

		return this.http.post(this.URL + `borrar.php`, { table: table, data: data }, { headers: httpHeaders });
	}

	createData(table: string, data: string) {

		const userToken = localStorage.getItem(environment.authTokenKey);
		const httpHeaders = new HttpHeaders({
			'Authorization': 'Bearer ' + userToken
		});

		return this.http.post(this.URL + `anadir.php`, { table: table, data: data }, { headers: httpHeaders });
	}

	updateData(table: string, values: string) {

		const userToken = localStorage.getItem(environment.authTokenKey);
		const httpHeaders = new HttpHeaders({
			'Authorization': 'Bearer ' + userToken
		});

		console.log(table);
		console.log(values);
		
		return this.http.post(this.URL + `actualizar.php`, { table: table, data: values }, { headers: httpHeaders });
	}

	async readSVG(url) {

		let respuesta = await this.http.post(this.URL + `images-svg.php`, { url: url }, { headers: new HttpHeaders({
		}) }).toPromise();
		//console.log(respuesta["data"]);
		return  respuesta["data"];
	}
}

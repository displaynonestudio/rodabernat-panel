// Angular
import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
// Components
import {BaseComponent} from './views/theme/base/base.component';
import {ErrorPageComponent} from './views/theme/content/error-page/error-page.component';
// Auth
import {AuthGuard} from './core/auth';
import { HomeComponent } from './pages/home/home.component';
import { UsuariosComponent } from './views/pages/usuarios/usuarios.component';
import { ClientesComponent } from './views/pages/clientes/clientes.component';
import { ClienteComponent } from './views/pages/clientes/cliente/cliente.component';
import { PedidosComponent } from './views/pages/pedidos/pedidos.component';

import { PresupuestosComponent } from './views/pages/presupuestos/presupuestos.component';
import { PresupuestoComponent } from './views/pages/presupuestos/presupuesto/presupuesto.component';

import { AlbaranesComponent } from './views/pages/albaranes/albaranes.component';
import { AlbaranComponent } from './views/pages/albaranes/albaran/albaran.component';

import { ArticulosComponent } from './views/pages/articulos/articulos.component';
import { ArticuloComponent } from './views/pages/articulos/articulo/articulo.component';

import { PartesComponent } from './views/pages/partes/partes.component';
import { ParteComponent } from './views/pages/partes/parte/parte.component';

import { PartesSinValComponent } from './views/pages/partes-sin-val/partes-sin-val.component';
import { ParteSinValComponent } from './views/pages/partes-sin-val/parte-sin-val/parte-sin-val.component';

import { UsuarioComponent } from './views/pages/usuarios/usuario/usuario.component';
import { RolesComponent } from './views/pages/roles/roles.component';
import { RolComponent } from './views/pages/roles/rol/rol.component';

import { PedidoComponent } from './views/pages/pedidos/pedido/pedido.component';
import { ConfiguradorComponent } from './pages/configurador/configurador.component';


const routes: Routes = [
	{ path: 'home/:familia', component: HomeComponent },
	{ path: 'configurador/:familia', component: ConfiguradorComponent },
	{ path: 'auth', loadChildren: () => import('./views/pages/auth/auth.module').then(m => m.AuthModule) },
	{ path: 'panel',  component: BaseComponent, canActivate: [AuthGuard], children: [
			{ path: 'dashboard', loadChildren: () => import('./views/pages/dashboard/dashboard.module').then(m => m.DashboardModule) },
			{ path: 'usuarios', component: UsuariosComponent },
			{ path: 'usuarios/:id', component: UsuarioComponent },

			{ path: 'presupuestos', component: PresupuestosComponent },
			{ path: 'presupuestos/:id', component: PresupuestoComponent },

			{ path: 'albaranes', component: AlbaranesComponent },
			{ path: 'albaranes/:id', component: AlbaranComponent },

			{ path: 'partes', component: PartesComponent },
			{ path: 'partes/:id', component: ParteComponent },

			{ path: 'articulos', component: ArticulosComponent },
			{ path: 'articulos/:id', component: ArticuloComponent },

			{ path: 'partes-sin-val', component: PartesSinValComponent },
			{ path: 'partes-sin-val/:id', component: ParteSinValComponent },

			{ path: 'clientes', component: ClientesComponent },
			{ path: 'clientes/:id', component: ClienteComponent },
			{ path: 'roles', component: RolesComponent },
			{ path: 'roles/:id', component: RolComponent },
			{ path: 'error/403', component: ErrorPageComponent, data: {
					type: 'error-v6',
					code: 403,
					title: '403... Access forbidden',
					desc: 'Looks like you don\'t have permission to access for requested page.<br> Please, contact administrator',
				},
			},
			{ path: 'error/:type', component: ErrorPageComponent },
			{ path: '', redirectTo: 'referencias', pathMatch: 'full' },
			{ path: '**', redirectTo: 'referencias', pathMatch: 'full' },
		],
	},

	{ path: '**', redirectTo: 'error/403', pathMatch: 'full' },
	{ path: '', redirectTo: 'panel', pathMatch: 'full' },
];

@NgModule({
	imports: [
		RouterModule.forRoot(routes),
	],
	exports: [RouterModule],
})
export class AppRoutingModule {
}
